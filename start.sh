#!/bin/bash

if [ "$(basename "$1")" == "dosbox.conf" ]; then
	cd "$(dirname "$1")"
elif [ -d "$1" ]; then
	cd "$1"
else
	echo "parameter should be dosbox.conf file or folder that contains this file"
	exit 1
fi

dosbox
