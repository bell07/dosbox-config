# dosbox-config

dosbox games configuration tree.


The tree contains the dosbox configuration for some games. 

Tested with dosbox-staging-0.78.1

If you go into game folder that includes the `dosbox.conf` file, and start dosbox from there, the file is used and the game starts.

`start.sh` does it for you. Means `/path/to/start.sh /path/to/Mario` runs dosbox inside Mario folder. The script is useful to setup "Emulator" in GameHub.

`dosbox-mapper.map`is used in dosbox.conf and contains the joystick keys mapping. This way the games are playeable with xbox compatible joypad.

Of course the games are not included into this repository because of license reasons. But look into `.gitignore` files which files or folders are not included.

## License
LGPL2
