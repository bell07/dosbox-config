#!/bin/sh

# Note: Use "cvt 720 960 60" and set the modeline into xorg.conf
# Calculation: native Y is 720 on my device. X = 720 *4/3 = 960
# Syntax is YxX for me because my panel uses upright modes and is rotated. Usual it is XxY
#xrandr --output eDP1 --mode 720x960_60.00
xrandr --output eDP1 --mode 720x960_60.00 --scale-from 480x640


function fullscreen_async() {
	sleep 1
#	wmctrl -i -r $(wmctrl -l | awk '/DOSBox-X/{print $1}') -b add,fullscreen
	wmctrl -r DOSBox -b add,fullscreen
}

cd "$(dirname "$0")"
rm fastseek.lut

fullscreen_async &
#dosbox-x -set char9=false -set showmenu=false -set windowposition=0,0 -fullscreen
#strangle -f 30 -k -v 2 gamemoderun dosbox-x -set char9=false -set showmenu=false -set windowposition=0,0
gamemoderun dosbox-x -set char9=false -set showmenu=false -set windowposition=0,0

# Reset xrandr
xrandr --output eDP1 --auto --scale 1x1
